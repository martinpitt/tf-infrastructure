variable "worker_tags" {
  description = "Tags to identify Testing Farm worker AWS instances"
  type        = map(string)
}
