---

#
# Add tag repository with HIGH priority, for installing latest dependencies of tested artifacts.
#
# The tag repository is updated by Koji. It contains all packages that got into a specific tag. The repository is sometimes
# also referred to as buildroot repository. The repo contains packages newer then the official repositories, they get these
# updates later.
#
# The reason for HIGH priority was based on discussion here: https://pagure.io/fedora-ci/general/issue/252#comment-737145
#
# The repository is ENABLED by default for the whole run of the test.
#
# Examples for Fedora:
# * rawhide contains all builds tagged into rawhide.
#
# The play gracefully skips adding a tag repository which is not reachable.
#
# Tag repository is enabled only on Fedora and CentOS Stream.
# See https://pagure.io/fedora-ci/general/issue/376 for discussion about the current agreement.
#

- name: Check for rawhide
  command: "rpm -q fedora-repos-rawhide"
  ignore_errors: true
  register: check_rawhide

- block:
    #
    # The tag repository name can be determined from the Fedora version. Detect rawhide
    # by checking for rawhide repository installed via 'fedora-repos-rawhide' rpm
    #
    - block:
        - name: Transform build target to a tag repository name (Rawhide)
          set_fact:
            tag_repository_name: "rawhide"
          when: check_rawhide.rc == 0

        - name: Transform build target to a tag repository name (Fedora non-Rawhide)
          set_fact:
            tag_repository_name: "f{{ ansible_distribution_version }}-build"
          when: check_rawhide.rc != 0

        - name: Create tag repository base url (Fedora)
          set_fact:
            tag_repository_url: "https://kojipkgs.fedoraproject.org/repos/{{ tag_repository_name }}/latest/{{ ansible_architecture }}/"

      #
      # Run block only on Fedora
      #

      when: ansible_distribution == "Fedora"

    #
    # Add tag repository for CentOS Stream 8
    #
    - name: Create tag repository base url (CentOS Stream 8)
      set_fact:
        tag_repository_name: "c8s-build"
        tag_repository_url: "https://kojihub.stream.centos.org/kojifiles/repos/c8s-build/latest/{{ ansible_architecture }}/"
      when: IMAGE_NAME|lower|regex_search('centos.stream.8')

    #
    # Add tag repository for CentOS Stream 9
    #
    - name: Create tag repository base url (CentOS Stream 9)
      set_fact:
        tag_repository_name: "c9s-build"
        tag_repository_url: "https://kojihub.stream.centos.org/kojifiles/repos/c9s-build/latest/{{ ansible_architecture }}/"
      when: IMAGE_NAME|lower|regex_search('centos.stream.9')

    #
    # Add tag repository for CentOS Stream 10
    #
    - name: Create tag repository base url (CentOS Stream 10)
      set_fact:
        tag_repository_name: "c10s-build"
        tag_repository_url: "https://kojihub.stream.centos.org/kojifiles/repos/c10s-build/latest/{{ ansible_architecture }}/"
      when: IMAGE_NAME|lower|regex_search('centos.stream.10')

    #
    # Check if tag repository exists.
    #
    # Note: We ignore the fact the repository does not exist and try to continue anyway. Note that not all builds
    #       we encounter might have tag repository available.
    #
    - name: "Check if tag repository {{ tag_repository_url }} exists"
      command: "bash -c 'curl --head -L {{ tag_repository_url }} | grep -o -E \"(200|403|404)\"'"
      retries: "{{ pkg_retry_count }}"
      delay: "{{ pkg_retry_delay }}"
      register: http_status_code

    #
    # Add repository only if the repository is reachable
    #
    - block:
        - name: Add tag repository
          template:
            src: "templates/tag.repo.j2"
            dest: /etc/yum.repos.d/tag-repository.repo

      when: http_status_code.stdout == "200"

    #
    # Be verbose about the fact the repository is unreachable
    #
    - debug:
        msg: "Skipping adding tag repository, because location {{ tag_repository_url }} was not found or is not working"
      when: http_status_code.stdout != "200"

  #
  # block: end
  #
  when:
    - ansible_distribution == "Fedora" or IMAGE_NAME|lower|regex_search('centos.stream')

#
# Be verbose about the fact we skipped this play
#
- debug:
    msg: |
      Skipping adding tag repository, because the distrubution is not Fedora or CentOS Stream.

  when: ansible_distribution != "Fedora" and not IMAGE_NAME|lower|regex_search('centos.stream')
